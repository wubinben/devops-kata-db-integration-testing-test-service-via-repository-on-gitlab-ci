package devops.katas.customers;

import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CustomerService {
    private CustomerRepository customerRepository;

    public CustomerService(CustomerRepository customerRepository) {
        this.customerRepository = customerRepository;
    }

    public Customer create(Customer customer) {
        return customerRepository.save(customer);
    }
;
    public Customer findById(Long id) {
        String errorMessage = "id" + id + "NotFound";
        return customerRepository.findById(id).orElse(new Customer(Long.valueOf(-1), errorMessage, errorMessage));
    }

    public List<Customer> findAll() {
        return customerRepository.findAll();
    }

    public void update(Customer newCustomer) {
        String errorMessage = "id" + newCustomer.getId() + "NotFound";
        Customer oldCustomer = customerRepository.findById(newCustomer.getId()).orElse(new Customer(Long.valueOf(-1), errorMessage, errorMessage));
        oldCustomer.setFirstName(newCustomer.getFirstName());
        oldCustomer.setLastName(newCustomer.getLastName());
        customerRepository.save(oldCustomer);
    }

    public void delete(Customer customer) {
        customerRepository.delete(customer);
    }
}
