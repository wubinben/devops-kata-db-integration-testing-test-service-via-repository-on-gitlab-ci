package devops.katas.customers;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface CustomerRepository extends JpaRepository<Customer, Long> {
    void deleteAll();
    Optional<Customer> findById(Long id);
    Customer save(Customer customer);
    List<Customer> findAll();
}