package devops.katas.customers;

import org.assertj.core.groups.Tuple;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest
public class CustomerServiceIT {
	@Autowired
	CustomerRepository customerRepository;

	@Autowired
	CustomerService customerService;

	@Test
	public void should_create_a_customer() {
		// given
		customerRepository.deleteAll();

		// when
		Long id = customerService.create(new Customer(Long.valueOf(0), "Ben", "Wu")).getId();

		// then
		Customer actualCustomer = customerRepository.findById(id).
				orElse(new Customer(Long.valueOf(-1), "id" + id + "NotFound", "id" + id + "NotFound"));
		assertThat(actualCustomer).isEqualToComparingFieldByField(new Customer(id, "Ben", "Wu"));

		// teardown
//		customerRepository.deleteAll();
	}

	@Test
	public void should_delete_a_customer() {
		// given
		customerRepository.deleteAll();
		Customer actualCustomer;
		Customer createdCustomer = customerService.create(new Customer(Long.valueOf(0), "Ben", "Wu"));

		// when
		customerService.delete(createdCustomer);

		// then
		String errorMessage = "id" + createdCustomer.getId() + "NotFound";
		actualCustomer = customerRepository.findById(createdCustomer.getId()).orElse(new Customer(Long.valueOf(-1), errorMessage, errorMessage));
		assertThat(actualCustomer).isEqualToComparingFieldByField(new Customer(Long.valueOf(-1), errorMessage, errorMessage));

		// teardown
//		customerRepository.deleteAll();
	}

	@Test
	public void should_update_a_customer() {
		// given
		customerRepository.deleteAll();
		Customer customer = new Customer(Long.valueOf(0), "Ben", "Wu");
		Customer createdCustomer = customerService.create(customer);
		createdCustomer.setFirstName("FirstBen");

		// when
		customerService.update(createdCustomer);

		// then
		String errorMessage = "id" + createdCustomer.getId() + "NotFound";
		Customer actualCustomer = customerRepository.findById(createdCustomer.getId()).orElse(new Customer(Long.valueOf(-1), errorMessage, errorMessage));
		assertThat(actualCustomer).isEqualToComparingFieldByField(new Customer(createdCustomer.getId(), "FirstBen", "Wu"));

		// teardown
//		customerRepository.deleteAll();
	}

	@Test
	public void should_find_a_customer_by_id_happy_path() {
		// given
		customerRepository.deleteAll();
		Long id = customerRepository.save(new Customer(Long.valueOf(0), "Ben", "Wu")).getId();

		// when
		Customer actualCustomer = customerService.findById(id);

		// then
		Customer expectedCustomer = new Customer(id, "Ben", "Wu");
		assertThat(actualCustomer).isEqualToComparingFieldByField(expectedCustomer);

		// teardown
//		customerRepository.deleteAll();
	}

	@Test
	public void should_find_a_customer_by_id_sad_path() {
		// given
		customerRepository.deleteAll();

		// when
		Customer actualCustomer = customerService.findById(Long.valueOf(-1));

		// then
		String errorMessage = "id-1NotFound";
		assertThat(actualCustomer.getFirstName()).isEqualTo(errorMessage);
		assertThat(actualCustomer.getLastName()).isEqualTo(errorMessage);

		// teardown
//		customerRepository.deleteAll();
	}

	@Test
	public void should_find_all_customers() {
		// given
		customerRepository.deleteAll();
		Long id1 = customerRepository.save(new Customer(Long.valueOf(0), "Ben", "Wu")).getId();
		Long id2 = customerRepository.save(new Customer(Long.valueOf(0), "Ben2", "Wu2")).getId();
		Long id3 = customerRepository.save(new Customer(Long.valueOf(0), "Ben3", "Wu3")).getId();

		// when
		List<Customer> actualCustomers = customerService.findAll();

		// then
		assertThat(actualCustomers).hasSize(3)
				.extracting("id", "firstName", "lastName")
				.contains(
						Tuple.tuple(id1, "Ben", "Wu"),
						Tuple.tuple(id2, "Ben2", "Wu2"),
						Tuple.tuple(id3, "Ben3", "Wu3")
				);

		// teardown
//		customerRepository.deleteAll();
	}

}
